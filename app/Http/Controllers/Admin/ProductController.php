<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Exports\ProductsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Product;
use App\Colour;
use App\Category;
use App\Http\Requests\StoreProduct;
use App\Http\Requests\ProductUpdateRequest;
use Session;
use DB;
use Yajra\Datatables\Datatables;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       
        return view('admin.product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $colour = Colour::get();
        $category = Category::get();
        return view('admin.product.create',compact('category','colour'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProduct $request)
    {
        //

        $data['product_name']   = $request->input('product_name');
        $data['product_description'] = $request->input('product_description');
        $data['product_code'] = $request->input('product_code');
        $data['product_price'] = $request->input('product_price');
        $data['discount_price'] = $request->input('discount_price');
        $data['product_image'] = $request->input('product_image');
        $data['product_colour'] = implode(',',$request->input("product_colour"));
        $data['product_category'] = $request->input('product_category');
        $data['product_subcategory'] = $request->input('product_subcategory');
         $data['product_status'] = 1;
       

         $productid = Product::create($data)->product_id;

         if($productid){

            $fields = array(
            'to' => '/topics/global',
            'notification' => array('click_action'=>'main_activity','title' => $data['product_name'], 'body' => strip_tags($data['product_description'])),
            'data' => array('param1' =>$productid ,'imageLink' =>$data['product_image'],'date'=>'12/2/2016')
            
        );
            $this->toFcm($fields);
         }

       Session::flash('message', 'You have successfully Insert Product.');
        return redirect()->route("product.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

         $product = DB::table('products')
        ->leftJoin('categories', 'products.product_category', '=', 'categories.category_id')
        ->leftJoin('sub_categories', 'products.product_subcategory', '=', 'sub_categories.subcategory_id')
        ->where('product_id','=',$id)
        ->get();
        foreach($product as $p){
            $colo = $p->product_colour;
        }
         $colourp = explode(',', $colo);
        foreach($colourp as $key=>$val){
            $colour[]= $val;
        }
         return view('admin.product.show',compact('product','colour'));
        
    }
    public function via($notifiable)
    {
        return ['fcm'];
    }
    public function toFcm($notifiable) 

    {

        $url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array(
            //AIzaSyCOAoIUioLD7vm4zvg-ETbZAZPzSR12VI4
            'Authorization: key=AAAA4bB1eag:APA91bF3IJe3hFEiVr_FPbz1SgTVSCcg0cCdy2AfoXLNXK2XwRakXMY9l2k3TGZB7oCfe7NSRLNheyddhirQ-O7mtoMTg5wjJqnJjxTs5W29hoWaO0_6RSo4MwJMrWM3aHKNXdqgQvWd',
            'Content-Type: application/json'
        );
       
        
 
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($notifiable));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
           // echo 'url failed'. curl_error($ch);
        }

        // Close connection
        curl_close($ch);
       // echo json_encode($fields1).'<br>';
        //echo $result;

       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $product = Product::FindOrFail($id);

         $colourp = explode(',', $product['product_colour']);
        foreach($colourp as $key=>$val){
            $procolour[]= $val;
        }
        
        $subcategory =  DB::select('select * from sub_categories where parent_category = :id', ['id' => $product->product_category]);
       
        $category = Category::get();
         $colour = Colour::get();
        return view('admin.product.edit',compact('product','procolour','category','subcategory','colour'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        //
        $data['product_name']   = $request->input('product_name');
        $data['product_description'] = $request->input('product_description');
        $data['product_code'] = $request->input('product_code');
        $data['product_price'] = $request->input('product_price');
        $data['discount_price'] = $request->input('discount_price');
        $data['product_image'] = $request->input('product_image');
        $data['product_colour'] = implode(',',$request->input("product_colour"));
        $data['product_category'] = $request->input('product_category');
        $data['product_subcategory'] = $request->input('product_subcategory');
        

        Product::findOrFail($id)->update($data);
       Session::flash('message', 'You have successfully Update Product.');
        return redirect()->route("product.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Product::destroy($id);

        Session::flash('message', 'You have successfully Delete Product.');
        return redirect()->route("product.index");
    }
    public function get_subcategory(Request $request)
    {
        $id = $request->input('categryid');
        $category = DB::select('select * from sub_categories where parent_category = :id', ['id' => $id]);
        return json_encode($category);
    }
    public function getenable(Request $request)
    {
        $product = $request->input('product_id');
        $productenable = Product::where('product_id','=',$product)->where('product_status','=',0)->update(['product_status' =>1]);
        return json_encode($productenable);
    }
    public function getdisable(Request $request)
    {
        $product = $request->input('product_id');
        $productenable = Product::where('product_id','=',$product)->where('product_status','=',1)->update(['product_status' =>0]);
        return json_encode($productenable);
    }

    public function export()
    {
        return Excel::download(new ProductsExport, 'products.xlsx');
    }

    public function getData()
    {
            $product = Product::with('category')->get();
        $table = DataTables::of($product)
            ->escapeColumns([])
            ->addColumn('no', function ($new) {
                return '';
            })
            ->editColumn('product_status',function($new){
                $b='';
                 if($new->product_status == 1){
                    $b .= '<button type="button" class="btn btn-primary btn-sm" onclick="productenable('.$new->product_id.')">Enable</button>';
                    $b .='<button type="button" onclick="productdisable('.$new->product_id.')" class="btn btn-default btn-sm">Disable</button>';
                 }else{
                    $b .= '<button type="button" onclick="productenable('.$new->product_id.')" class="btn btn-default btn-sm">Enable</button>';

                    $b .= '<button type="button" onclick="productdisable('.$new->product_id.')" class="btn btn-danger btn-sm" >Disable</button>';
                            
                 }
                return $b; 
            })
            ->editColumn('product_image',function($new){
                return "<img src='".$new->product_image."' width='100px'/>";
            })
            ->addColumn('action', function ($new) {
                $btn = '<a href="'. route('product.show', $new) .'" class="btn btn-info btn-sm"> <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';

                $btn .= '<a href="'. route('product.edit', $new) .'" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>';

                $btn .= '<a href="'. route('product.delete', $new) .'" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> </a>';
               
                return "<div class='action-column'>" . $btn . "</div>";
            });

        return $table->make(true);
    }
}
