<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreColour;
use App\Colour;
use Session;
class ColourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $colour = Colour::get();
        return view('admin.colour.index',compact('colour'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.colour.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreColour $request)
    {
        //
        $data['colour_name'] = $request->input('colour_name');
        $data['colour_code'] = $request->input('colour_code');
        $colour = Colour::create($data);
       Session::flash('message', 'You have successfully Insert Colour.');
        return redirect()->route("colour.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $colour = Colour::FindOrFail($id);
        return view('admin.colour.edit',compact('colour'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Colour::findOrFail($id)->update($request->all());
       
        Session::flash('message', 'You have successfully updated Colour.');
        return redirect()->route('colour.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         Colour::destroy($id);

        Session::flash('message', 'You have successfully Delete Colour.');
        return redirect()->route("colour.index");
    }
}
