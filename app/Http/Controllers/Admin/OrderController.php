<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\SubCategory;
use App\ProductOrder;
use App\OrderUser;
use DB;
use Session;
class OrderController extends Controller
{
    //

    public function index()
    {
    	$order = OrderUser::get();
    	return view('admin.order.index',compact('order'));
    }
    public function show($id)
    {
    	$productenable = OrderUser::where('order_id','=',$id)->where('order_view','=',0)->update(['order_view' =>1]);

    	$orderlist = DB::table('product_orders')
    	 ->leftJoin('products', 'product_orders.order_productid', '=', 'products.product_id')
    	 ->leftJoin('categories', 'products.product_category', '=', 'categories.category_id')
        ->leftJoin('sub_categories', 'products.product_subcategory', '=', 'sub_categories.subcategory_id')
    	->where('orderid','=',$id)->get();
    	return view('admin.order.show',compact('orderlist'));
    }
    public function newOrder()
    {
    	 $neworder = OrderUser::where('order_view','=',0)->count();
    	 return json_encode($neworder);
    }
    public function orderPackaged(Request $request)
    {
        $order = $request->input('order_id');
        $packaged = OrderUser::where('order_id','=',$order)->update(['order_package' =>1]);
        return json_encode($packaged);
    }
    public function orderPackage(Request $request)
    {
        $order = $request->input('order_id');
        $packaged = OrderUser::where('order_id','=',$order)->update(['order_package' =>0]);
        return json_encode($packaged);
    }
    public function destroy($id)
    {
        //
        DB::table('product_orders')->where('orderid', '=', $id)->delete();
        OrderUser::destroy($id);

        Session::flash('message', 'You have successfully Delete Product.');
        return redirect()->route("order");
    }
}
