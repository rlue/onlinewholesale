<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubCategory;
use App\Category;
use App\Http\Requests\StoreSubCategory;
use Session;
use DB;
class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $subcategory = DB::table('sub_categories')
            ->leftJoin('categories', 'sub_categories.parent_category', '=', 'categories.category_id')
            ->get();
       
        return view('admin.subcategory.index',compact('subcategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $parent = Category::get();
        return view('admin.subcategory.create',compact('parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubCategory $request)
    {
        //
        $data = [
        'subcategory_name'      => $request->input("subcategory_name"),
        'parent_category'     => $request->input('parent_category'),
       
        ];

        $user = SubCategory::create($data);
       Session::flash('message', 'You have successfully Insert SubCategory.');
        return redirect()->route("subcategory.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $subcategory = SubCategory::FindOrFail($id);
        $parent = Category::get();
        return view('admin.subcategory.edit',compact('subcategory','parent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = [
        'subcategory_name'      => $request->input("subcategory_name"),
        'parent_category'     => $request->input('parent_category'),
       
        ];
        SubCategory::findOrFail($id)->update($data);
       
        Session::flash('message', 'You have successfully updated SubCategory.');
        return redirect()->route('subcategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         SubCategory::destroy($id);

        Session::flash('message', 'You have successfully Delete SubCategory.');
        return redirect()->route("subcategory.index");
    }
}
