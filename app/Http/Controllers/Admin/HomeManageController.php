<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HomeManage;
use App\SubCategory;

use Session;
class HomeManageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     
    public function index()
    {
        //
        
        $homemanage = HomeManage::with('subcategory')->get();
       
        return view('admin.homemanage.index',compact('homemanage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $subcategory = SubCategory::get();
        return view('admin.homemanage.create',compact('subcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data['subcategoryid'] = $request->input('subcategoryid');
        
        $homemanage = HomeManage::create($data);
       Session::flash('message', 'You have successfully Insert homemanage.');
        return redirect()->route("homemanage.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $subcategory = SubCategory::get();
        $homemanage = HomeManage::FindOrFail($id);
        return view('admin.homemanage.edit',compact('homemanage','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
       HomeManage::where('homemanage_id','=',$id)->update($request->all());
       
        Session::flash('message', 'You have successfully updated HomeManage.');
        return redirect()->route('homemanage.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        HomeManage::destroy($id);

        Session::flash('message', 'You have successfully Delete HomeManage.');
        return redirect()->route("homemanage.index");
    }
}
