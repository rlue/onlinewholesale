<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\SubCategory;
use App\Product;
use DB;
class CategoryController extends Controller
{
    //
    public function index()
    {
    	$category = Category::get();
    	$getcat=[];
    	$subcat = [];
    	foreach($category as $c){
    		$sub = SubCategory::where('parent_category','=',$c->category_id)->get();

    		foreach($sub as $s){
    			$subcat = [
    			'subcategory_id' => $s->subcategory_id,
    			'subcategory_name' => $s->subcategory_name
    		];
    		}

    		$getcat[] =
    		 [
    			'category_id' => $c->category_id,
    			'category_name' => $c->category_name,
    			'subcategory' => $sub
    		];
    	}
    	return response()->json($getcat);
    }

    public function getproduct(Request $request)
    {
    	$id = $request->input('category_id');

    	$product = Product::where('product_category','=',$id)->orderBy('product_id','desc')->paginate(3);

        $productlist = [];

        foreach($product as $p){
             $categorylist = [];
        $procolour = [];
        $subcategorylist = [];
                $category = Category::where('category_id','=',$p->product_category)->get();
                foreach($category as $c){
                    $categorylist[] = [
                        'category_id' => $c->category_id,
                        'category_name' => $c->category_name
                    ];
                }
                $subcategory = SubCategory::where('subcategory_id','=',$p->product_subcategory)->get();
                foreach($subcategory as $s){
                    $subcategorylist[] = [
                        'subcategory_id' => $s->subcategory_id,
                        'subcategory_name' => $s->subcategory_name
                    ];
                }
                $colour  = $colourp = explode(',', $p->product_colour);
                foreach($colour as $key=>$val){
            $procolour[]= $val;
                 }
            $productlist[] = [
                'product_id' => $p->product_id,
                'product_name' => $p->product_name,
                'product_description' => $p->product_description,
                'product_code' => $p->product_code,
                'product_colour' => $procolour,
                'product_price' => $p->product_price,
                'product_status' => $p->product_status,
                'discount_price' => $p->discount_price,
                'product_image' => $p->product_image,
                'product_category' => $categorylist,
                'product_subcategory' => $subcategorylist
            ]; 
        }
    	$result = [
                'numberPage' => $product->lastPage(),
                'data' => $productlist
            ];
        return response()->json($result);
    }
    public function getwithSubcategory(Request $request)
    {
        $id = $request->input('subcategory_id');

        $product = Product::where('product_subcategory','=',$id)->orderBy('product_id','desc')->paginate(40);

        $productlist = [];

        foreach($product as $p){
             $categorylist = [];
        $procolour = [];
        $subcategorylist = [];
                $category = Category::where('category_id','=',$p->product_category)->get();
                foreach($category as $c){
                    $categorylist[] = [
                        'category_id' => $c->category_id,
                        'category_name' => $c->category_name
                    ];
                }
                $subcategory = SubCategory::where('subcategory_id','=',$p->product_subcategory)->get();
                foreach($subcategory as $s){
                    $subcategorylist[] = [
                        'subcategory_id' => $s->subcategory_id,
                        'subcategory_name' => $s->subcategory_name
                    ];
                }
                $colour  = $colourp = explode(',', $p->product_colour);
                foreach($colour as $key=>$val){
            $procolour[]= $val;
                 }
            $productlist[] = [
                'product_id' => $p->product_id,
                'product_name' => $p->product_name,
                'product_description' => $p->product_description,
                'product_code' => $p->product_code,
                'product_colour' => $procolour,
                'product_price' => $p->product_price,
                'product_status' => $p->product_status,
                'discount_price' => $p->discount_price,
                'product_image' => $p->product_image,
                'product_category' => $categorylist,
                'product_subcategory' => $subcategorylist
            ]; 
            
        }
        $result = [
                'numberPage' => $product->lastPage(),
                'data' => $productlist
            ];
        return response()->json($result);
    }
    public function getdetail(Request $request)
    {
    	$id = $request->input('product_id');

    	$product = Product::where('product_id','=',$id)->get();

        $productlist = [];

        foreach($product as $p){
             $categorylist = [];
        $procolour = [];
        $subcategorylist = [];
                $category = Category::where('category_id','=',$p->product_category)->get();
                foreach($category as $c){
                    $categorylist[] = [
                        'category_id' => $c->category_id,
                        'category_name' => $c->category_name
                    ];
                }
                $subcategory = SubCategory::where('subcategory_id','=',$p->product_subcategory)->get();
                foreach($subcategory as $s){
                    $subcategorylist[] = [
                        'subcategory_id' => $s->subcategory_id,
                        'subcategory_name' => $s->subcategory_name
                    ];
                }
                $colour  = $colourp = explode(',', $p->product_colour);
                foreach($colour as $key=>$val){
            $procolour[]= $val;
                 }
            $productlist[] = [
                'product_id' => $p->product_id,
                'product_name' => $p->product_name,
                'product_description' => $p->product_description,
                'product_code' => $p->product_code,
                'product_colour' => $procolour,
                'product_price' => $p->product_price,
                'product_status' => $p->product_status,
                'discount_price' => $p->discount_price,
                'product_image' => $p->product_image,
                'product_category' => $categorylist,
                'product_subcategory' => $subcategorylist
            ]; 
        }
        return response()->json($productlist);
    }
    public function gethomepage()
    {

    	$product = Product::orderBy('product_id','desc')->limit(20)->get();

        $productlist = [];
       // $productlist[] = ['version' => 1.0];
        foreach($product as $p){
             $categorylist = [];
        $procolour = [];
        $subcategorylist = [];
                $category = Category::where('category_id','=',$p->product_category)->get();
                foreach($category as $c){
                    $categorylist[] = [
                        'category_id' => $c->category_id,
                        'category_name' => $c->category_name
                    ];
                }
                $subcategory = SubCategory::where('subcategory_id','=',$p->product_subcategory)->get();
                foreach($subcategory as $s){
                    $subcategorylist[] = [
                        'subcategory_id' => $s->subcategory_id,
                        'subcategory_name' => $s->subcategory_name
                    ];
                }
                $colour  = $colourp = explode(',', $p->product_colour);
                foreach($colour as $key=>$val){
            $procolour[]= $val;
                 }
            
            $productlist[] = [
                'product_id' => $p->product_id,
                'product_name' => $p->product_name,
                'product_description' => $p->product_description,
                'product_code' => $p->product_code,
                'product_colour' => $procolour,
                'product_price' => $p->product_price,
                'product_status' => $p->product_status,
                'discount_price' => $p->discount_price,
                'product_image' => $p->product_image,
                'product_category' => $categorylist,
                'product_subcategory' => $subcategorylist
            ]; 
        }
        return response()->json($productlist);
    }
}
