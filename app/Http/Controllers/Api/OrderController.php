<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\OrderStore;
use App\Product;
use App\Category;
use App\SubCategory;
use App\ProductOrder;
use App\OrderUser;
use DB;
use Carbon\Carbon;

class OrderController extends Controller
{
    //

    public function index(OrderStore $request)
    {
    	$input = Input::all();
    	  $data = [
         'order_user'      => $request->input("order_user"),
         'order_phone'      => $request->input("order_phone"),
         'order_address'     => $request->input("order_address"),
         'order_package'    => 0,
         'order_view'       => 0,
         'order_date'      => Carbon::now(),
       
         ];
      //   //return response()->json($data, 201);
         $id = OrderUser::create($data)->order_id;
        	
        $condition =     json_decode( $input['order_productid'] , true);
            $ordercount =     json_decode( $input['order_count'] , true);
          
            
            foreach ($condition as $key => $cond) {
                 $student = new ProductOrder;
                 $student->order_productid = $condition[$key];
                 $student->order_count = $ordercount[$key];
                 $student->orderid = $id;
                 $student->save();
             }
        if($id){
        	 $datas = [
                    'data' => 1,
                    'result' => 'Order Successful'
                ];
                return response()->json($datas);
        }else{
        	 $datas = [
                    'data' => 0,
                    'result' => 'order fail'
                ];
                return response()->json($datas);
        }
            
    }
}
