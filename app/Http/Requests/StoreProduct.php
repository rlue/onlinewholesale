<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        'product_name'  => 'required',
        'product_description'  => 'required',
        'product_code'  => 'required',
        'product_price'  => 'required',
        'discount_price'  => 'required',
        'product_image'  => 'required',
        'product_colour'  => 'required',
        'product_category'  => 'required',
        ];
    }
}
