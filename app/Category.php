<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\SubCategory;
class Category extends Model
{
    //
     protected $fillable = [
        'category_name','category_img'
    ];
    protected $primaryKey = 'category_id';
    
    protected $dates = ['created_at', 'updated_at'];

    public function subcategory()
    {
    	return $this->belongsTo(SubCategory::class);
    }
}
