<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeManage extends Model
{
    //
     protected $fillable = [
        'subcategoryid'
    ];
    protected $primaryKey = 'homemanage_id';
    
  

    public function subcategory()
    {
    	return $this->belongsTo(SubCategory::class,'subcategoryid');
    }
}
