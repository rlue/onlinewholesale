<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderUser extends Model
{
    //
     protected $fillable = [
        'order_user','order_phone','order_date','order_view','order_package','order_address'
    ];
    protected $primaryKey = 'order_id';
}
