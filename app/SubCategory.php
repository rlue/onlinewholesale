<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
class SubCategory extends Model
{
    //
     protected $fillable = [
        'subcategory_name','parent_category'
    ];
    protected $primaryKey = 'subcategory_id';

    public function category()
    {
    	return $this->belongsTo(Category::class,'parent_category');
    }
}
