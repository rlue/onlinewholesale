<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOrder extends Model
{
    //
    protected $fillable = [
        'orderid','order_productid','order_count'
    ];
    
}
