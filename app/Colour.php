<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colour extends Model
{
    //
    protected $fillable = [
        'colour_code','colour_name'
    ];
    protected $primaryKey = 'colour_id';
}
