<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\SubCategory;
class Product extends Model
{
    //
     protected $fillable = [
        'product_name','product_description','product_code','product_price','product_image','product_colour','product_category','product_subcategory','product_status','discount_price'
    ];
    protected $primaryKey = 'product_id';

    public function category()
    {
    	return $this->belongsTo(Category::class,'product_category');
    }
     public function subcategory()
    {
    	return $this->belongsTo(SubCategory::class,'product_subcategory');
    }
}
