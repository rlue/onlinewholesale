<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductColoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_colours', function (Blueprint $table) {

             $table->integer('productid')->unsigned();
            $table->integer('colourid')->unsigned();

            $table->foreign('productid')
                ->references('product_id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('colourid')
                ->references('colour_id')
                ->on('colours')
                ->onDelete('cascade');

            $table->primary(['productid', 'colourid']);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_colours');
    }
}
