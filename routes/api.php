<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'],function(){

	Route::get('/homepage', 'Api\CategoryController@gethomepage');
Route::get('/category', 'Api\CategoryController@index');
Route::post('/getproduct', 'Api\CategoryController@getproduct');
Route::post('/getwithSubcategory', 'Api\CategoryController@getwithSubcategory');
Route::post('/getdetail','Api\CategoryController@getdetail');

Route::post('/register','Api\UserController@register');
Route::post('/order','Api\OrderController@index');
});
