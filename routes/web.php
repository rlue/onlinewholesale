<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');
Auth::routes();


Route::get('/backend/login','Auth\LoginController@showLoginForm')->name('backend.login');
Route::post('/backend/login','Auth\LoginController@login');
Route::group(['middleware' => 'auth', 'prefix' => 'backend'], function () {
	
	Route::get("/", 'Admin\UserController@index');
	Route::get("users", 'Admin\UserController@index')->name("users.index");
	Route::get("users/create", 'Admin\UserController@create')->name("users.create");
	Route::get("users/{id}/edit", 'Admin\UserController@edit')->name("users.edit");
	Route::get("users/delete/{id}", 'Admin\UserController@destroy')->name("users.delete");
	Route::patch('users/update/{id}',"Admin\UserController@update")->name('users.update');
	Route::post('users/store', 'Admin\UserController@store')->name("users.store");


	//role route
	Route::get("roles", 'Admin\RoleController@index')->name("roles.index");
	Route::get("roles/create", 'Admin\RoleController@create')->name("roles.create");
	Route::get("roles/{id}/edit", 'Admin\RoleController@edit')->name("roles.edit");
	Route::get("roles/delete/{id}", 'Admin\RoleController@destroy')->name("roles.delete");
	Route::patch('roles/update/{id}',"Admin\RoleController@update")->name('roles.update');
	Route::post('roles/store', 'Admin\RoleController@store')->name("roles.store");

	//Permission route
	Route::get("permissions", 'Admin\PermissionController@index')->name("permissions.index");
	Route::get("permissions/create", 'Admin\PermissionController@create')->name("permissions.create");
	Route::get("permissions/{id}/edit", 'Admin\PermissionController@edit')->name("permissions.edit");
	Route::get("permissions/delete/{id}", 'Admin\PermissionController@destroy')->name("permissions.delete");
	Route::patch('permissions/update/{id}',"Admin\PermissionController@update")->name('permissions.update');
	Route::post('permissions/store', 'Admin\PermissionController@store')->name("permissions.store");
	// category route
	Route::get("category", 'Admin\CategoryController@index')->name("category.index");
	
	Route::get("category/{id}/edit", 'Admin\CategoryController@edit')->name("category.edit");
	Route::get("category/delete/{id}", 'Admin\CategoryController@destroy')->name("category.delete");
	Route::patch('category/update/{id}',"Admin\CategoryController@update")->name('category.update');
	
	//sub categry route
	Route::get("subcategory", 'Admin\SubCategoryController@index')->name("subcategory.index");
	
	Route::get("subcategory/{id}/edit", 'Admin\SubCategoryController@edit')->name("subcategory.edit");
	Route::get("subcategory/delete/{id}", 'Admin\SubCategoryController@destroy')->name("subcategory.delete");
	Route::patch('subcategory/update/{id}',"Admin\SubCategoryController@update")->name('subcategory.update');
	Route::get("order/delete/{id}", 'Admin\OrderController@destroy')->name("order.delete");
	//Colour route
	Route::get("colour", 'Admin\ColourController@index')->name("colour.index");
	
	Route::get("colour/{id}/edit", 'Admin\ColourController@edit')->name("colour.edit");
	Route::get("colour/delete/{id}", 'Admin\ColourController@destroy')->name("colour.delete");
	Route::patch('colour/update/{id}',"Admin\ColourController@update")->name('colour.update');
	//HOme Manage route
	Route::get("homemanage", 'Admin\HomeManageController@index')->name("homemanage.index");
	
	Route::get("homemanage/{id}/edit", 'Admin\HomeManageController@edit')->name("homemanage.edit");
	Route::get("homemanage/delete/{id}", 'Admin\HomeManageController@destroy')->name("homemanage.delete");
	Route::patch('homemanage/update/{id}',"Admin\HomeManageController@update")->name('homemanage.update');
	Route::get("homemanage/create", 'Admin\HomeManageController@create')->name("homemanage.create");
	//Product route
	Route::get("product/getData","Admin\ProductController@getData")->name('product.getData');
	Route::get("product", 'Admin\ProductController@index')->name("product.index");
	
	Route::get("product/{id}/edit", 'Admin\ProductController@edit')->name("product.edit");
	Route::get("product/delete/{id}", 'Admin\ProductController@destroy')->name("product.delete");
	Route::patch('product/update/{id}',"Admin\ProductController@update")->name('product.update');
	
	Route::get('product/show/{id}',"Admin\ProductController@show")->name("product.show");
	Route::get('product/export',"Admin\ProductController@export")->name("product.export");

	Route::post("getsubcategory", 'Admin\ProductController@get_subcategory')->name("getsubcategory");
	Route::post("getenable", 'Admin\ProductController@getenable')->name("getenable");
	Route::post("getdisable", 'Admin\ProductController@getdisable')->name("getdisable");

	Route::post("orderPackaged","Admin\OrderController@orderPackaged")->name('orderPackaged');
	Route::post("orderPackage","Admin\OrderController@orderPackage")->name('orderPackage');
	
	Route::get("order","Admin\OrderController@index")->name('order');
	Route::get("order/show/{id}","Admin\OrderController@show")->name('order.show');
	Route::get("getneworder","Admin\OrderController@newOrder")->name('getneworder');

	Route::group(['middleware' => ['permission:create']],function(){
		Route::get("category/create", 'Admin\CategoryController@create')->name("category.create");
		Route::post('category/store', 'Admin\CategoryController@store')->name("category.store");

		Route::get("subcategory/create", 'Admin\SubCategoryController@create')->name("subcategory.create");
		Route::post('subcategory/store', 'Admin\SubCategoryController@store')->name("subcategory.store");

		Route::get("colour/create", 'Admin\ColourController@create')->name("colour.create");
		Route::post("homemanage/store", 'Admin\HomeManageController@store')->name("homemanage.store");
		Route::post('colour/store', 'Admin\ColourController@store')->name("colour.store");

		Route::get("product/create", 'Admin\ProductController@create')->name("product.create");
		Route::post('product/store', 'Admin\ProductController@store')->name("product.store");
	});

});