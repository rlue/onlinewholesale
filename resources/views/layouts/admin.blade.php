<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Natthame | WholeSale in Myanmar </title>

    <!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
     
     <link href="{{ asset('css/nprogress.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
       <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">
       <link href="{{ asset('css/summernote.css') }}" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Natthame WholeSale</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">

                <img src="img/banner.png" alt="..." class="img-circle profile_img">

              </div>
              <div class="profile_info">
              
                <span>Welcome,</span>
                <h2>{{ Auth::user()->name }}</h2>
             
                
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                 @if(Auth::user()->is_super)
                  <li><a><i class="fa fa-edit"></i> User Manage <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      
                      <li><a href="{{route('users.index')}}">User List</a></li>
                      <li><a href="{{route('roles.index')}}">Role List</a></li>
                      <li><a href="{{route('permissions.index')}}">Permission List</a></li>
                    </ul>
                  </li>
                  @endif
                  <li><a><i class="fa fa-edit"></i> Home Manage <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('homemanage.index')}}">Home Category</a></li>
                     
                    </ul>
                  </li>
                 <li><a><i class="fa fa-edit"></i> Product Manage <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('product.index')}}">Product List</a></li>
                      <li><a href="{{route('category.index')}}">Category List</a></li>
                      <li><a href="{{route('subcategory.index')}}">SubCategory List</a></li>
                      <li><a href="{{route('colour.index')}}">Colour List</a></li>
                    </ul>
                  </li>
                  @can('order')
                  <li><a><i class="fa fa-edit"></i> Order Manage <span class="fa fa-chevron-down"></span> <span class="badge bg-green order_new" ></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('order')}}">Order List</a></li>
                     </ul>
                  </li>
                  @endcan
                 
                </ul>
              </div>
              

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <!-- <a id="menu_toggle"><i class="fa fa-bars"></i></a> -->
              </div>
                @if (Auth::guest())
                  <h2>Login</h2>
              @else
                 <ul class="nav navbar-nav navbar-right">
                <li class="">

                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                    <!-- <img src="images/img.jpg" alt=""> -->{{ Auth::user()->name }}

                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green order_new" ></span>
                  </a>

                 
                </li>
              </ul>
              @endif
             
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        

         @yield('content')
       
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">

            Powered by <a href="http://itechmm.com">Itechmm.com</a>

          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/summernote.min.js') }}"></script>
    <!-- jQuery -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('js/bootstrap-progressbar.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/nprogress.js') }}"></script>
  
    <!-- Custom Theme Scripts -->
    <script src="{{ asset('js/custom.min.js') }}"></script>
    <script type="text/javascript">
      $(document).ready(function() {
  $('#summernote').summernote();
  $.ajax({
          headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
            type: "get",
            url: '<?php echo route('getneworder') ?>',
            dataType:'json',
            success: function( msg ) {
               $('.order_new').append(msg);
            }
        });
});
    </script>
<script type="text/javascript">
  
  function getsubcat()
  {
    var catid = $('select[name=product_category]').val()
    
    $.ajax({
          headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
            type: "POST",
            url: '<?php echo route('getsubcategory') ?>',
            data: {categryid: catid},
            dataType:'json',
            success: function( msg ) {
                $('#parentcat option').remove();
                $.each(msg, function (i, item) {
                  
                  $('#parentcat').append($('<option>', { 
                      value: item.subcategory_id,
                      text : item.subcategory_name 
                  }));
              });
            }
        });
  }
  function productenable($id)
  {

    var productid = $id;
      $.ajax({
          headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
            type: "POST",
            url: '<?php echo route('getenable') ?>',
            data: {product_id: productid},
            dataType:'json',
            success: function( msg ) {
               if(msg){
                  alert('Enable Success');
                  location.reload();
               }else{
                  alert('have been enable');
               }
                
            }
        });
  }
  function productdisable($id)
  {

    var productid = $id;
      $.ajax({
          headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
            type: "POST",
            url: '<?php echo route('getdisable') ?>',
            data: {product_id: productid},
            dataType:'json',
            success: function( msg ) {
               if(msg){
                  alert('Disable Success');
                  location.reload();
               }else{
                  alert('have been Disable');
               }
                
            }
        });
  }
  function orderPackaged($id)
  {
       var orderid = $id;
      $.ajax({
          headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
            type: "POST",
            url: '<?php echo route('orderPackaged') ?>',
            data: {order_id: orderid},
            dataType:'json',
            success: function( msg ) {
               if(msg){
                  alert('Packaged Success');
                  location.reload();
               }else{
                  alert('have been Click');
               }
                
            }
        });
  }
  function orderPackage($id)
  {
       var orderid = $id;
      $.ajax({
          headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
            type: "POST",
            url: '<?php echo route('orderPackage') ?>',
            data: {order_id: orderid},
            dataType:'json',
            success: function( msg ) {
               if(msg){
                  alert('not packaged order');
                  location.reload();
               }else{
                  alert('have been Click');
               }
                
            }
        });
  }
</script>
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script type="text/javascript">
  $('#lfm').filemanager('image');
</script>
 @stack('scripts')
  </body>
</html>
