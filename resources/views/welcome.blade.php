<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Natthame | WholeSale in Myanmar </title>

    <!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <style type="text/css">
        .logo_show{
            margin-top: 150px;
        }
        .customer_show{

            text-align: center;
        }
    </style> 
  </head>

  <body>
   <div class="container">
   <div class="row logo_show">
       <div class="col-sm-5">

       </div>
       <div class="col-sm-2">
            <img class="img-responsive" src="{{asset('img/banner.png')}}">
       </div>
       <div class="col-sm-5">

       </div>
   </div>
   <div class="row">
       <div class="col-sm-12 customer_show">
           <h3>မိတ္ေဟာင္းမိတ္သစ္ Customer မ်ား အားလံုး</h3>
           <p>Natthame Mobile Application တြင္္ လြယ္ကူအဆင္ေျပစြာ၀ယ္ယူအားေပးနိုင္ပါၿပီ ရွင့္</p>
           <p>Download ျပဳလုပ္ရန္ <a href="https://play.google.com/store/apps/details?id=com.naylinn.angelshopping">Natthame App</a></p>
       </div>
   </div>
   </div>
<script src="{{ asset('js/jquery.min.js') }}"></script>

    <!-- jQuery -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
   
    <!-- FastClick -->
   

  </body>
</html>
