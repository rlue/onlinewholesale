@extends('layouts.admin')

@section('content')
    

    <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">

                <h3>Order Manage<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <!-- <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span> -->
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>Manage <small>Product</small></h2>
                   
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>User Name</th>
                          <th>Phone</th>
                          <th>Date</th>
                          <th>Address</th>
                           @can('order_package')
                          <th>Order Package</th>
                          @endcan
                          <th>View</th>
                         
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($order as $p)
                        <tr>
                          <td>{{$p->order_user}}
                          @if($p->order_view == 0)
                            <span class="btn btn-danger btn-xs"> New </span>
                          @endif
                          </td>
                          <td>{{$p->order_phone}}</td>

                          <td>{{$p->order_date}}</td>
                          <td>{!! $p->order_address !!}</td>
                          @can('order_package')
                          <td>
                            <div class="btn-group" role="group" aria-label="...">
                            @if($p->order_package == 1)
                            <button type="button" class="btn btn-primary" onclick="orderPackaged({{$p->order_id}})" >Packaged</button>
                            <button type="button" onclick="orderPackage({{$p->order_id}})" class="btn btn-default">Not Package</button>
                            @else
                            <button type="button" onclick="orderPackaged({{$p->order_id}})" class="btn btn-default">Packaged</button>
                            <button type="button" onclick="orderPackage({{$p->order_id}})" class="btn btn-danger" >Not Package</button>
                            @endif
                          </div>
                          </td>
                          @endcan
                          <td>
                            <a href="{{route('order.show',$p->order_id)}}" class="btn btn-success">View</a>
                            @if(Auth::user()->is_super)
                            <a href="{{route('order.delete',$p->order_id)}}" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
                            @endif
                          </td>
                          
                          
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
					
					
                 
            </div>
          </div>
        </div>
@endsection


