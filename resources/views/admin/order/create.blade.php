@extends('layouts.admin')

@section('content')
    

   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Product Manage</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Product Create Form <small>different form elements</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                     @if (count($errors) > 0)
       <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
                    <form method="post" action="{{ route('product.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="Product Name">Product Name</label>
                            <input type="text" class='form-control' name="product_name" placeholder="Product Name">
                        </div>
                        <div class="form-group">
                            <label for="product_description">Product Description</label>
                            <textarea name="product_description" class="form-control" id="summernote"></textarea>
                            
                        </div>
                        <div class="form-group">
                            <label for="product_code">Product Code</label>
                           <input type="text" class='form-control' name="product_code" placeholder="Product Code">
                        </div>
                        <div class="form-group">
                            <label for="product_price">Product Price</label>
                            <input type="text" class='form-control' name="product_price" placeholder="Product Price">
                        </div>
                        <div class="form-group">
                            <label for="discount_price">Discount Price</label>
                            <input type="text" class='form-control' name="discount_price" placeholder="Discount Price">
                        </div>
                        <div class="form-group">
                            <label for="Product Name">Product Image</label>
                           
                        </div>
                        <div class="input-group">
                        
                           <span class="input-group-btn">
                             <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                               <i class="fa fa-picture-o"></i> Choose
                             </a>
                           </span>
                           <input id="thumbnail" class="form-control" type="text" name="product_image">
                         </div>
                         <div class="form-group">
                            <label for="Product Name">Colour</label>
                            @foreach($colour as $o)
                            <input type="checkbox" name="product_colour[]" placeholder="Product Colour" value="{{$o->colour_code}}"> {{$o->colour_name}}
                            @endforeach
                        </div>
                         <div class="form-group">
                            <label for="product_category">Category</label>
                            <select name="product_category" class="form-control" onchange="getsubcat();">
                            @foreach($category as $p)
                              <option value="{{$p->category_id}}">{{$p->category_name}}</option>
                            @endforeach
                            </select>
                        </div>
                         <div class="form-group">
                            <label for="product_subcategory">SubCategory</label>
                             <select name="product_subcategory" class="form-control" id="parentcat">
                           
                            </select>
                        </div>

                       
                        <div class="form-group">
                           <a href="{{route('product.index')}}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancel</a>
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save</button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

           
         
          </div>
        </div>
@endsection


