@extends('layouts.admin')

@section('content')
    

    <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">

                <h3>Product Manage<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <!-- <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span> -->
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>Manage <small>Product</small></h2>
                    <a href="{{route('product.export')}}" class="btn btn-info pull-right"><span class="glyphicon glyphicon-save-file" aria-hidden="true"></span>Product Export</a>
                    @can('create')
                    <a href="{{route('product.create')}}" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create Product</a>
                   @endcan
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  
                    <table id="product-table" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                      
                          <th>Product Name</th>
                          <th>Product Code</th>
                          <th>Product Image</th>
                          <th>Price</th>
                          <th> Status</th>
                          
                          <th>Action</th>
                         
                        </tr>
                      </thead>


                     
                    </table>
                  </div>
                </div>
              </div>

              
					
					
                 
            </div>
          </div>
        </div>
@endsection

@push('scripts')
<script>
$(function() {
    $('#product-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('product.getData') !!}',
        columns: [
            { data: 'product_name', name: 'product_name' },
            { data: 'product_code', name: 'product_code' },
            { data: 'product_image', name: 'product_image' },
            { data: 'product_price', name: 'product_price' },
            { data: 'product_status', name: 'product_status' },
            {data: 'action', name: 'action', orderable: false, searchable: false},
            
        ]
    });
});
</script>
@endpush
