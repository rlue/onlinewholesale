@extends('layouts.admin')

@section('content')
    

    <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">

                <h3>Product Manage<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <!-- <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span> -->
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>Detail <small>Product</small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  @foreach($product as $p)
                   <div class="col-sm-6">
                     <img src="{{$p->product_image}}" class="img-responsive" />
                   </div>
                   <div class="col-sm-6">
                      <h3>{{$p->product_name}}</h3>
                      <p><span><strong>Description :</strong></span> {!! $p->product_description !!}</p>      
                      <p><span><strong>Code :</strong></span> {{$p->product_code}}</p>
                      <p><span><strong>Price :</strong></span> {{$p->product_price}}</p>
                      <p><span><strong>Discount :</strong></span> {{$p->discount_price}}</p>
                      <p><span><strong>Colour :</strong></span>
                      
                     
                        @foreach($colour as $key=>$val)
                          <button style="padding:6px;background: {{$val}}"></button>
                        @endforeach
                       </p>
                      <p><span><strong>Category :</strong></span> {{$p->category_name}}</p>
                      <p><span><strong>SubCategory :</strong></span> {{$p->subcategory_name}}</p>
                                   
                   </div>
                   @endforeach
                  </div>
                </div>
              </div>

              
					
					
                 
            </div>
          </div>
        </div>
@endsection


