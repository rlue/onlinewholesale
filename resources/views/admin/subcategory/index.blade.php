@extends('layouts.admin')

@section('content')
    

    <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">

                <h3>SubCategory Manage<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <!-- <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span> -->
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>Manage <small>SubCategory</small></h2>
                    @can('create')
                    <a href="{{route('subcategory.create')}}" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create SubCategory</a>
                    @endcan
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Sub Category Name</th>
                          <th>Category Name</th>
                          @can('update')
                          <th>Edit</th>
                          @endcan
                          @can('delete')
                          <th>Delete</th>
                          @endcan
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($subcategory as $c)
                        <tr>
                          <td>{{$c->subcategory_name}}</td>
                          <td>{{$c->category_name}}</td>
                          @can('update')
                          <td>
                          
                          <a href="{{route('subcategory.edit',$c->subcategory_id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>
                         
                          </td>
                           @endcan
                           @can('delete')
                          <td>
                          
                          <a href="{{route('subcategory.delete',$c->subcategory_id)}}" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
                          
                          </td>
                          @endcan
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
					
					
                 
            </div>
          </div>
        </div>
@endsection


