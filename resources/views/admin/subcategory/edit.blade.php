@extends('layouts.admin')

@section('content')
    

   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>SubCategory Management</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>SubCategory Manageent Edit <small>different form elements</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                     @if (count($errors) > 0)
       <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
                    <form method="post" action="{{route('subcategory.update',$subcategory->subcategory_id)}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="subcategory_name">SubCategory Name *</label>
                        <input type="text" class='form-control' name="subcategory_name" value="{{$subcategory->subcategory_name}}" placeholder="SubCateogory Name">
                    </div>
                   
                    <div class="form-group">
                           <label for="Category Name">ParentCategory Name</label>
                        <select name="parent_category" class="form-control">
                            @foreach($parent as $p)
                                @if($p->category_id == $subcategory->parent_category)
                              <option value="{{$p->category_id}}" selected="selected">{{$p->category_name}}</option>
                              @else
                               <option value="{{$p->category_id}}">{{$p->category_name}}</option>
                              @endif
                            @endforeach
                        </select>
                    </div>
                  
                    <form action="{{ route('subcategory.store', $subcategory->subcategory_id) }}"
                >
                    {{ csrf_field() }}
                    {{ method_field("patch") }}
                    <a href="{{route('subcategory.index')}}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancel</a>
                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save</button>
                </form>
                </form>
                  </div>
                </div>
              </div>
            </div>

           
         
          </div>
        </div>
@endsection


