<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'fcm' => [

        'key' => 'AAAA4bB1eag:APA91bF3IJe3hFEiVr_FPbz1SgTVSCcg0cCdy2AfoXLNXK2XwRakXMY9l2k3TGZB7oCfe7NSRLNheyddhirQ-O7mtoMTg5wjJqnJjxTs5W29hoWaO0_6RSo4MwJMrWM3aHKNXdqgQvWd'

     ],

];
